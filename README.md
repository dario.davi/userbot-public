## Description

Prime number generator

## Running the app in a Docker container

```bash
docker build . -t primes
docker run --net=host primes
```

## Classic installation

```bash
$ npm install
```

## Running the app

```bash
$ npm start
```

## Test and coverage

```bash
# unit tests and e2e tests
$ npm test
```

## Note

Ho svolto l'esercizio usando Typescript e il framework Nest.js.  
I nomi dei file sono dati secondo la convenzione di Angular, usata anche da Nest.js.  
I file contenenti i test rispecchiano la convenzione secondo cui dovrebbero trovarsi nella stessa directory dei file testati.  
Se vengono eseguiti i test, viene generato un report di code coverage in html nella directory "coverage".  
Per verificare i risultati nei test, ho usato una libreria di terze parti che genera numeri primi.  
Anche se è una bad practice, ho aggiunto al repository un file .env per facilitare l'avvio dell'applicazione.  
L'implementazione dell'algoritmo è nel file "primes-generator.ts": per fare una cosa diversa dal solito ho scelto di implementarlo usando gli stream di RxJS.   

