import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {PrimesGenerator} from './lib/primes-generator';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, PrimesGenerator]
})
export class AppModule {}
