import {Injectable} from '@nestjs/common';
import {PrimesGenerator} from './lib/primes-generator';
import {Observable} from 'rxjs';

@Injectable()
export class AppService {
  constructor(readonly primesGenerator: PrimesGenerator) {}

  getPrimes(n: number): Observable<number[]> {
    return this.primesGenerator.generate$(n);
  }
}
