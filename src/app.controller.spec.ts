import {Test, TestingModule} from '@nestjs/testing';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import * as sequences from 'integer-sequences';
import {PrimesGenerator} from './lib/primes-generator';

describe('AppController', () => {
  let appController: AppController;
  const MAX = 10000;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [AppService, PrimesGenerator]
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return correct result', async () => {
      expect(await appController.getNPrimeNumbers(MAX).toPromise()).toEqual(
        sequences.prime.sequence(MAX)
      );
    });
  });
});
