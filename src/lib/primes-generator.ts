import {generate, Observable, partition} from 'rxjs';
import {Injectable} from '@nestjs/common';
import {
  bufferCount,
  filter,
  flatMap,
  isEmpty,
  mapTo,
  tap
} from 'rxjs/operators';

@Injectable()
export class PrimesGenerator {
  public generate$(max: number): Observable<number[]> {
    let go = true;

    return generate(
      2,
      (x) => go,
      (x) => ++x
    ).pipe(
      filter((x) => x === 2 || x % 2 !== 0),
      flatMap(this.filterPrimes),
      bufferCount(max),
      tap((x) => (go = false))
    );
  }

  private filterPrimes(currentValue) {
    const limit = Math.round(Math.sqrt(currentValue));
    const [noRemainder, remainder] = partition(
      generate(
        3,
        (x) => x <= limit,
        (x) => x + 2
      ),
      (div) => currentValue % div === 0
    );

    return noRemainder.pipe(isEmpty(), filter(Boolean), mapTo(currentValue));
  }
}
