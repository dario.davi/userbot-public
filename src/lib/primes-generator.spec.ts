import {PrimesGenerator} from './primes-generator';
import * as sequences from 'integer-sequences';

describe('PrimesGenerator', () => {
  const generator = new PrimesGenerator();
  const MAX = 10000;

  describe('generate$', () => {
    it('should return correct result', async () => {
      expect(await generator.generate$(MAX).toPromise()).toEqual(
        sequences.prime.sequence(MAX)
      );
    });
  });
});
