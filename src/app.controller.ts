import {Controller, Get, Query} from '@nestjs/common';
import {AppService} from './app.service';
import {Observable} from 'rxjs';

@Controller('primi')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getNPrimeNumbers(@Query('q') n: number): Observable<number[]> {
    return this.appService.getPrimes(n);
  }
}
