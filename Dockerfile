FROM node:12-slim

ENV PORT 3000

WORKDIR /app

COPY package* ./

RUN npm install

COPY . .

ENV NODE_ENV production

RUN npm run build

EXPOSE 3000

ENTRYPOINT ["node"]
CMD ["dist/main.js"]
